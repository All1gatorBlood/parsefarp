from selenium import webdriver
from selenium.common import NoSuchElementException
from selenium.webdriver.common.by import By
from selenium.webdriver.chrome.service import Service
import time
import logging
from datetime import datetime
import tkinter as tk
from tkinter import filedialog, scrolledtext, messagebox
import threading
from urllib.parse import quote, unquote
import json
import sqlite3
from openpyxl import Workbook, load_workbook
from openpyxl.utils import get_column_letter
from openpyxl.styles import Font
from tkcalendar import DateEntry
import re

def export_to_excel(search_word, date_from, date_to):
    date_pattern = r"\d{4}-\d{2}-\d{2}"
    if not re.match(date_pattern, date_from) or not re.match(date_pattern, date_to):
        messagebox.showerror("Ошибка", "Неверный формат даты. Введите дату в формате 'гггг-мм-дд'.")
        return

    conn = sqlite3.connect("database.db")
    c = conn.cursor()

    c.execute("SELECT * FROM result WHERE query = ? AND timestamp >= ? AND timestamp <= ?",
              (search_word, date_from, date_to))
    rows = c.fetchall()

    filename = "results.xlsx"
    try:
        wb = load_workbook(filename)
    except FileNotFoundError:
        wb = Workbook()

    if search_word in wb.sheetnames:
        ws = wb[search_word]
        ws.delete_rows(2, ws.max_row)
    else:
        ws = wb.create_sheet(title=search_word)


    headers = ['Запрос', 'ID', 'Наименование', 'Цена', 'Наличие', 'Владелец', 'На момент']
    for col_num, header in enumerate(headers, 1):
        col_letter = get_column_letter(col_num)
        ws[f"{col_letter}1"] = header
        ws[f"{col_letter}1"].font = Font(bold=True)


    for row_num, row in enumerate(rows, 2):
        for col_num, value in enumerate(row, 1):
            col_letter = get_column_letter(col_num)
            ws[f"{col_letter}{row_num}"] = value

    wb.save(filename)
    if export_completed:
        messagebox.showinfo("Выгрузка в Excel", f"Файл {filename} сохранен")
    conn.close()


def create_database():
    conn = sqlite3.connect("database.db")
    c = conn.cursor()
    c.execute('''CREATE TABLE IF NOT EXISTS result
                 (query TEXT, id TEXT, name TEXT, price FLOAT, availability TEXT, owner TEXT, timestamp TIMESTAMP)''')
    conn.commit()
    conn.close()

def save_to_database(query, rows):
    conn = sqlite3.connect("database.db")
    c = conn.cursor()
    for row in rows:
        c.execute("INSERT INTO result VALUES (?, ?, ?, ?, ?, ?, ?)", (unquote(query), row[0], row[1], row[2], row[3], row[4], row[5]))
    conn.commit()
    conn.close()

def read_config_file():
    file_path = filedialog.askopenfilename(filetypes=[("Config files", "*.json")])
    with open(file_path, "r") as file:
        config_data = json.load(file)
        config_text.delete(1.0, tk.END)
        config_text.insert(tk.END, json.dumps(config_data, indent=4))

def create_webdriver(config):
    if config["use_options"].lower() == "y":
        options = webdriver.ChromeOptions()
        options.add_argument('--headless')  # Запуск Chrome в режиме без графического интерфейса
        options.add_argument('--no-sandbox')
        options.add_argument('--disable-dev-shm-usage')
        service = Service(config["chrome_driver_path"])
        return webdriver.Chrome(options=options, service=service)
    else:
        service = Service(config["chrome_driver_path"])
        return webdriver.Chrome(service=service)

def send_search_request(query, driver):
    url = f"https://www.farpost.ru/vladivostok/dir?query={query}"
    driver.get(url)
    time.sleep(3)

    rows = []
    count_element = driver.find_element(By.XPATH, '//*[@id="itemsCount_placeholder"]')
    count = int(count_element.get_attribute('data-count'))

    print(f'Количество полученных строк: {count}')
    console_text.insert(tk.END, f"Количество полученных строк: {count}\n")

    for i in range(0, min(count+2, 50)):
        present = "В наличии"
        try:
            id = driver.find_element(By.XPATH,f"//*[@id='bulletins']/div[1]/table/tbody/tr[{i}]").get_attribute("data-doc-id")
            name = driver.find_element(By.XPATH,f'//*[@id="bulletins"]/div[1]/table/tbody/tr[{i}]/td/div/div/div[2]/div[2]/div[1]/a').text
            price = driver.find_element(By.XPATH,f'//*[@id="bulletins"]/div[1]/table/tbody/tr[{i}]/td/div/div/div[2]/div[1]/div/div/div/span').text
            try:
                price = float(price.replace(" ", "").replace("₽", ""))
            except ValueError:
                present = 'Нет в наличии'
                price = driver.find_element(By.XPATH,f'//*[@id="bulletins"]/table[1]/tbody/tr[{i}]/td/div/div/div[2]/div[1]/div[1]/div/div/span').text
            try:
                owner = driver.find_element(By.XPATH,f'//*[@id="bulletins"]/div[1]/table/tbody/tr[{i}]/td/div/div/div[2]/div[3]/div/div/div/div[2]/span').text
            except NoSuchElementException:
                owner = ''
            current_time = datetime.now().strftime('%Y-%m-%d')
            rows.append([id, name, price, present, owner, current_time])
        except NoSuchElementException:
            try:
                id = driver.find_element(By.XPATH,f"//*[@id='bulletins']/div[1]/table/tbody/tr[{i}]").get_attribute("data-doc-id")
                name = driver.find_element(By.XPATH,f'//*[@id="bulletins"]/div[1]/table/tbody/tr[{i}]/td/div/div/div[2]/div[2]/div[1]/a').text
                price = driver.find_element(By.XPATH,f'//*[@id="bulletins"]/div[1]/table/tbody/tr[{i}]/td/div/div/div[2]/div[1]/div/div/div/div[1]/span[1]').text
                try:
                    price = float(price.replace(" ", "").replace("₽", ""))
                except ValueError:
                    present = 'Нет в наличии'
                    price = driver.find_element(By.XPATH,f'//*[@id="bulletins"]/table/tbody/tr[{i}]/td/div/div/div[2]/div[1]/div/div/div/div[1]/span[1]').text
                try:
                    owner = driver.find_element(By.XPATH,f'//*[@id="bulletins"]/div[1]/table/tbody/tr[{i}]/td/div/div/div[2]/div[3]/div/div/div/div[2]/span').text
                except NoSuchElementException:
                    owner = ''
                current_time = datetime.now().strftime('%Y-%m-%d')
                rows.append([id, name, price, present, owner, current_time])
            except NoSuchElementException:
                try:
                    id = driver.find_element(By.XPATH,f"//*[@id='bulletins']/div[1]/table/tbody/tr[{i}]").get_attribute("data-doc-id")
                    name = driver.find_element(By.XPATH,f'//*[@id="bulletins"]/div[1]/table/tbody/tr[{i}]/td/div/div/div[2]/div[2]/div[1]/a').text
                    price = driver.find_element(By.XPATH,f'//*[@id="bulletins"]/div[1]/table/tbody/tr[{i}]/td/div/div/div[2]/div[1]/div[1]/div/div/div[1]/span[1]').text
                    try:
                        price = float(price.replace(" ", "").replace("₽", ""))
                    except ValueError:
                        present = 'Нет в наличии'
                        price = driver.find_element(By.XPATH,f'//*[@id="bulletins"]/table[1]/tbody/tr[{i}]/td/div/div/div[2]/div[1]/div[1]/div/div/span').text
                    try:
                        owner = driver.find_element(By.XPATH,f'//*[@id="bulletins"]/div[1]/table/tbody/tr[{i}]/td/div/div/div[2]/div[3]/div/div/div/div[2]/span').text
                    except NoSuchElementException:
                        owner = ''
                    current_time = datetime.now().strftime('%Y-%m-%d')
                    rows.append([id, name, price, present, owner, current_time])
                except NoSuchElementException:
                    i += 1
    return rows


def start_program():
    global is_running
    is_running = True
    config = eval(config_text.get("1.0", tk.END))
    queries = config["queries"]
    create_database()
    logging.basicConfig(filename='log.txt', level=logging.ERROR)
    start_button.config(state="disabled")

    def process_queries():
        for query in queries:
            if not is_running:
                break
            query = quote(query, safe='')
            driver = create_webdriver(config)
            rows = send_search_request(query, driver)
            save_to_database(query, rows)
            driver.quit()
            time.sleep(3)

            console_text.insert(tk.END, f"Query: " + unquote(query) + "\n")
            console_text.insert(tk.END, f"Rows: {len(rows)}\n")
        console_text.insert(tk.END, "----- END -----\n")
        start_button.config(state="normal")

    thread = threading.Thread(target=process_queries)
    thread.start()

def stop_program():
    global is_running
    is_running = False
    start_button.config(state="normal")


def export_to_excel_window():

    export_window = tk.Toplevel(root)

    export_window.title("Выгрузка в Excel")
    export_window.geometry("200x200")

    search_word_label = tk.Label(export_window, text="Слово поиска:")
    search_word_label.pack()

    search_word_entry = tk.Entry(export_window)
    search_word_entry.pack()

    date_from_label = tk.Label(export_window, text="Дата 'от' (гггг-мм-дд):")
    date_from_label.pack()
    date_from_entry = DateEntry(export_window, date_pattern="yyyy-mm-dd")
    date_from_entry.pack()

    date_to_label = tk.Label(export_window, text="Дата 'до' (гггг-мм-дд):")
    date_to_label.pack()
    date_to_entry = DateEntry(export_window, date_pattern="yyyy-mm-dd")
    date_to_entry.pack()


    def export_to_excel_button():
        search_word = search_word_entry.get()
        date_from = date_from_entry.get()
        date_to = date_to_entry.get()

        if search_word == "":
            export_all_to_excel(date_from, date_to)
        else:
            export_to_excel(search_word, date_from, date_to)

        search_word_entry.delete(0, tk.END)
        date_from_entry.delete(0, tk.END)
        date_to_entry.delete(0, tk.END)

        export_window.destroy()

    export_button = tk.Button(export_window, text="Выгрузить в Excel", command=export_to_excel_button)
    export_button.pack()

def export_all_to_excel(date_from, date_to):
    global export_completed
    conn = sqlite3.connect("database.db")
    c = conn.cursor()

    c.execute("SELECT DISTINCT query FROM result")
    queries = c.fetchall()
    export_completed = False
    for query in queries:
        query = query[0]
        export_to_excel(query, date_from, date_to)
    export_completed = True
    messagebox.showinfo("Выгрузка в Excel", f"Файл result сохранен")
    conn.close()

root = tk.Tk()

root.title("parseFort")
root.resizable(width=False, height=False)




menu_bar = tk.Menu(root)

file_menu = tk.Menu(menu_bar, tearoff=0)
file_menu.add_command(label="Выгрузить в Excel", command=export_to_excel_window)
file_menu.add_separator()
file_menu.add_command(label="Выход", command=root.quit)

menu_bar.add_cascade(label="Меню", menu=file_menu)

root.config(menu=menu_bar)

config_label = tk.Label(root, text="Config:")
config_label.pack()

config_text = tk.Text(root, height=10, width=50)
config_text.pack()

read_button = tk.Button(root, text="Read Config", command=read_config_file)
read_button.pack()

start_button = tk.Button(root, text="Start", command=start_program)
start_button.pack()

stop_button = tk.Button(root, text="Stop", command=stop_program)
stop_button.pack()

console_label = tk.Label(root, text="Console:")
console_label.pack()

console_text = scrolledtext.ScrolledText(root, height=10, width=50)
console_text.pack()

root.mainloop()